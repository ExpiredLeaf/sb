﻿using Microsoft.EntityFrameworkCore;
using SB.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.DB.Data
{
    public class SBContext : DbContext
    {
        public static SBContext Context { get; internal set; }

        public SBContext(DbContextOptions<SBContext> options) : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Products> Products { get; set; }
        public DbSet<Extras> Extras { get; set; }
        public DbSet<OrderItems> OrderItems { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<OrderItemExtras> OrderItemsExtra { get; set; }
        
        
    }
}

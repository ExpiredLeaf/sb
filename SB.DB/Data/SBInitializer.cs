﻿using SB.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.DB.Data
{
    public class SBInitializer
    {
        public static void Initialize(SBContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            SBContext.Context = context;

            if (context.Categories.Any())
            {
                return;
            }

            var categories = new List<Category>
            {
                new Category { Name = "Espresso Drinks"},
                new Category { Name = "Brewed Coffee"},
                new Category { Name = "Tea"},
            };
            categories.ForEach(item => context.Categories.Add(item));
            context.SaveChanges();

            var extras = new List<Extras>
            {
                new Extras { Name = "Cinnamon", Price = 1.99M, Stock = 10 },
                new Extras { Name = "Yellow Sugar", Price = 1.99M, Stock = 10 },
                new Extras { Name = "Syrup", Price = 1.99M, Stock = 10 },
                new Extras { Name = "Whipped Cream", Price = 1.99M, Stock = 10 }
            };
            extras.ForEach(item => context.Extras.Add(item));
            context.SaveChanges();

            var teaCategory = context.Categories.FirstOrDefault(x => x.Name == "Tea");
            if (teaCategory != null)
            {
                var teas = new List<Products>
                {
                    new Products { Name = "Mint", Price = 1.99M, Stock = 10, Category = teaCategory, CategoryId = teaCategory.Id},
                    new Products { Name = "Chamomile Herbal", Price = 1.99M, Stock = 10, Category = teaCategory, CategoryId = teaCategory.Id},
                    new Products { Name = "Earl Grey", Price = 1.99M, Stock = 10, Category = teaCategory, CategoryId = teaCategory.Id}
                };
                teas.ForEach(item => context.Products.Add(item));
                context.SaveChanges();
            }

            var brewedCategory = context.Categories.FirstOrDefault(x => x.Name == "Brewed Coffee");
            if (brewedCategory != null)
            {
                var brews = new List<Products>
                {
                    new Products { Name = "Filter Coffee", Price = 1.99M, Stock = 10, Category = brewedCategory, CategoryId = brewedCategory.Id},
                    new Products { Name = "Caffe Misto", Price = 1.99M, Stock = 10, Category = brewedCategory, CategoryId = brewedCategory.Id}
                };
                brews.ForEach(item => context.Products.Add(item));
                context.SaveChanges();
            }

            var espressoCategory = context.Categories.FirstOrDefault(x => x.Name == "Brewed Coffee");
            if (espressoCategory != null)
            {
                var espressos = new List<Products>
                {
                    new Products { Name = "Latte", Price = 1.99M, Stock = 10, Category = espressoCategory, CategoryId = espressoCategory.Id},
                    new Products { Name = "Mocha", Price = 1.99M, Stock = 10, Category = espressoCategory, CategoryId = espressoCategory.Id},
                    new Products { Name = "Machiato", Price = 1.99M, Stock = 10, Category = espressoCategory, CategoryId = espressoCategory.Id},
                    new Products { Name = "Capuccino", Price = 1.99M, Stock = 10, Category = espressoCategory, CategoryId = espressoCategory.Id},
                    new Products { Name = "Americano", Price = 1.99M, Stock = 10, Category = espressoCategory, CategoryId = espressoCategory.Id},
                    new Products { Name = "Espresso", Price = 1.99M, Stock = 10, Category = espressoCategory, CategoryId = espressoCategory.Id}
                };
                espressos.ForEach(item => context.Products.Add(item));
                context.SaveChanges();
            }
        }
    }
}

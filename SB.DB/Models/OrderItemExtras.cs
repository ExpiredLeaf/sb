﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.DB.Models
{
    public class OrderItemExtras
    {
        public Guid Id { get; set; }
        public Extras Extra { get; set; }
        public OrderItems OrderItem { get; set; }
        public Guid ExtraId { get; set; }
        public Guid OrderItemId { get; set; }
    }
}

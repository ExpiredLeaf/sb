﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.DB.Models
{
    public class Orders
    {
        public Guid Id { get; set; }
        public bool Completed { get; set; }
        public ICollection<OrderItems> OrderItems { get; set; }
    }
}

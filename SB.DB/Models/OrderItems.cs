﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.DB.Models
{
    public class OrderItems
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid OrderId { get; set; }
        public Products Product { get; set; }
        public Orders Order { get; set; }
        public ICollection<OrderItemExtras> Extras { get; set; }
    }
}

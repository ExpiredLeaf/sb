﻿using SB.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Interfaces
{
    public interface ICategoryBL
    {
        public string Add(Category category);
        public List<Category> ListAll();
        Category GetById(Guid id);
        string Update(Guid id, Category category);
        string Delete(Guid id);
    }
}

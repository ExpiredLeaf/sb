﻿using SB.BL.Objects.Request;
using SB.BL.Objects.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Interfaces
{
    public interface IOrdersBL
    {
        public CreateOrderResponse CreateOrder();
        string AddItemToOrder(Guid id, AddItemToOrderRequest request);
        CompleteOrderResponse CompleteOrder(Guid id, CompleteOrderRequest request);
        string RemoveItemFromOrder(Guid orderId, Guid itemId);
        string CancelOrder(Guid id);
        OrderDetailsResponse GetOrderDetails(Guid id);
    }
}

﻿using SB.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Interfaces
{
    public interface IProductsBL
    {
        string Add(Products product);
        List<Products> ListAll();
        Products GetById(Guid id);
        string Update(Guid id, Products product);
        string Delete(Guid id);
    }
}

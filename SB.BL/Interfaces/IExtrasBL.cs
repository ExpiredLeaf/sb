﻿using SB.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Interfaces
{
    public interface IExtrasBL
    {
        public string Add(Extras extra);

        public Extras GetById(Guid id);

        public List<Extras> ListAll();

        public string Update(Guid id, Extras category);

        public string Delete(Guid id);
    }
}

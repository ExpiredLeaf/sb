﻿using Microsoft.Extensions.Logging;
using SB.BL.Interfaces;
using SB.DB.Data;
using SB.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Impl
{
    public class CategoryBL : ICategoryBL
    {
        private readonly ILogger<CategoryBL> _logger;

        public CategoryBL(ILogger<CategoryBL> logger) 
        {
            _logger = logger;
        }

        public string Add(Category category)
        {
            if (category == null)
            {
                return "Failed";
            }
            lock (SBContext.Context)
            {
                SBContext.Context.Categories.Add(category);
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }

        public Category GetById(Guid id)
        {
            return SBContext.Context.Categories.FirstOrDefault(c => c.Id == id);
        }

        public List<Category> ListAll()
        {
            return SBContext.Context.Categories.ToList();
        }

        public string Update(Guid id, Category category)
        {
            lock (SBContext.Context)
            {
                var categoryToUpdate = SBContext.Context.Categories.FirstOrDefault(c => c.Id == id);
                if (categoryToUpdate == null)
                {
                    return "Failed";
                }
                categoryToUpdate.Name = category.Name;
                SBContext.Context.Categories.Update(categoryToUpdate);
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }

        public string Delete(Guid id)
        {
            lock (SBContext.Context)
            {
                SBContext.Context.Categories.Remove(GetById(id));
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }
    }
}

﻿using Microsoft.Extensions.Logging;
using SB.BL.Interfaces;
using SB.DB.Data;
using SB.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Impl
{
    public class ExtrasBL : IExtrasBL
    {
        private readonly ILogger<ExtrasBL> _logger;

        public ExtrasBL(ILogger<ExtrasBL> logger)
        {
            _logger = logger;
        }

        public string Add(Extras extra)
        {
            if (extra == null)
            {
                return "Failed";
            }
            lock (SBContext.Context)
            {
                SBContext.Context.Extras.Add(extra);
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }

        public Extras GetById(Guid id)
        {
            return SBContext.Context.Extras.FirstOrDefault(c => c.Id == id);
        }

        public List<Extras> ListAll()
        {
            return SBContext.Context.Extras.ToList();
        }

        public string Update(Guid id, Extras extra)
        {
            lock (SBContext.Context)
            {
                var extraToUpdate = SBContext.Context.Extras.FirstOrDefault(c => c.Id == id);
                if (extraToUpdate == null)
                {
                    return "Failed";
                }
                extraToUpdate.Name = extra.Name;
                SBContext.Context.Extras.Update(extraToUpdate);
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }

        public string Delete(Guid id)
        {
            lock (SBContext.Context)
            {
                SBContext.Context.Extras.Remove(GetById(id));
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }
    }
}

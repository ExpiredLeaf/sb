﻿using Microsoft.Extensions.Logging;
using SB.BL.Interfaces;
using SB.DB.Data;
using SB.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Impl
{
    public class ProductsBL : IProductsBL
    {
        private readonly ILogger<ProductsBL> _logger;

        public ProductsBL(ILogger<ProductsBL> logger)
        {
            _logger = logger;
        }

        public string Add(Products product)
        {
            if (product == null)
            {
                return "Failed";
            }
            lock (SBContext.Context)
            {
                SBContext.Context.Products.Add(product);
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }

        public Products GetById(Guid id)
        {
            return SBContext.Context.Products.FirstOrDefault(c => c.Id == id);
        }

        public List<Products> ListAll()
        {
            return SBContext.Context.Products.ToList();
        }

        public string Update(Guid id, Products product)
        {
            lock (SBContext.Context)
            {
                var productsToUpdate = SBContext.Context.Products.FirstOrDefault(c => c.Id == id);
                if (productsToUpdate == null)
                {
                    return "Failed";
                }
                productsToUpdate.Name = product.Name;
                SBContext.Context.Products.Update(productsToUpdate);
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }

        public string Delete(Guid id)
        {
            lock (SBContext.Context)
            {
                SBContext.Context.Products.Remove(GetById(id));
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }
    }
}

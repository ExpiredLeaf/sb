﻿using Microsoft.Extensions.Logging;
using SB.BL.Interfaces;
using SB.BL.Objects.Request;
using SB.BL.Objects.Response;
using SB.BL.Objects.Response.OrderDetails;
using SB.DB.Data;
using SB.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Impl
{
    public class OrdersBL : IOrdersBL
    {
        private readonly ILogger<OrdersBL> _logger;

        public OrdersBL(ILogger<OrdersBL> logger)
        {
            _logger = logger;
        }

        public string AddItemToOrder(Guid id, AddItemToOrderRequest request)
        {
            var order = LoadFullOrder(id);
            if (order == null)
            {
                return "Failed - No Order for Given Id";
            }
            if (order.OrderItems == null)
            {
                order.OrderItems = new List<OrderItems>();
            }
            var productToAdd = SBContext.Context.Products.FirstOrDefault(o => o.Id == request.ProductId);
            var requestedExtrasToAdd = request.ExtrasIds.Select(extraId => SBContext.Context.Extras.FirstOrDefault(extra => extra.Id == extraId));
            var extrasToAdd = requestedExtrasToAdd.Select(x => new OrderItemExtras { ExtraId = x.Id });
            var orderItemToAdd = new OrderItems
            {
                ProductId = productToAdd.Id,
                Product = productToAdd,
                Extras = extrasToAdd.ToArray()
            };
            lock (SBContext.Context)
            {
                order.OrderItems.Add(orderItemToAdd);
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }

        public string CancelOrder(Guid id)
        {
            var order = LoadFullOrder(id);
            if (order == null)
            {
                return "Failed - No Order for Given Id";
            }
            lock (SBContext.Context)
            {
                SBContext.Context.Orders.Remove(order);
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }

        public CompleteOrderResponse CompleteOrder(Guid id, CompleteOrderRequest request)
        {
            var order = LoadFullOrder(id);
            if (order == null)
            {
                return new CompleteOrderResponse { Result = "Failed - No Order for Given Id" };
            }
            if (order.Completed)
            {
                return new CompleteOrderResponse { Result = "Failed - Order already completed" };
            }
            var orderTotal = GetOrderTotal(order);
            if (request.AmountPaid < orderTotal)
            {
                return new CompleteOrderResponse { Result = "Failed - Not enough amount paid" };
            }
            lock (SBContext.Context)
            {
                foreach (var item in order.OrderItems)
                {
                    if (item.Product.Stock <= 0)
                    {
                        return new CompleteOrderResponse
                        {
                            Result = String.Format("Failed - Not enough {0} in stock", item.Product.Name)
                        };
                    }
                    foreach (var itemExtra in item.Extras)
                    {
                        if (itemExtra.Extra.Stock <= 0)
                        {
                            return new CompleteOrderResponse
                            {
                                Result = String.Format("Failed - Not enough {0} in stock", itemExtra.Extra.Name)
                            };
                        }
                        itemExtra.Extra.Stock--;
                    }
                    item.Product.Stock--;
                }
                order.Completed = true;
                SBContext.Context.SaveChanges();
            }
            return new CompleteOrderResponse { Result = "Success", AmountChange = orderTotal - request.AmountPaid };
        }

        public CreateOrderResponse CreateOrder()
        {
            try
            {
                var newOrder = new Orders();
                lock (SBContext.Context)
                {
                    SBContext.Context.Orders.Add(newOrder);
                    SBContext.Context.SaveChanges();
                }
                return new CreateOrderResponse { Id = newOrder.Id, Result = "Success" };
            }
            catch (Exception ex)
            {
                return new CreateOrderResponse { Result = "Failed" };
            }
        }

        public OrderDetailsResponse GetOrderDetails(Guid id)
        {
            var order = LoadFullOrder(id);
            if (order == null)
            {
                new OrderDetailsResponse { Result = "Failed - No Order for Given Id" };
            }

            return new OrderDetailsResponse
            {
                Result = "Success",
                Products = order.OrderItems.Select(orderItem => new OrderProduct
                {
                    ProductName = orderItem.Product.Name,
                    ProductPrice = orderItem.Product.Price,
                    OrderExtras = orderItem.Extras.Select(itemExtra => new OrderExtra
                    {
                        ExtraName = itemExtra.Extra.Name,
                        ExtraPrice = itemExtra.Extra.Price
                    })
                }),
                Completed = order.Completed,
                Total = GetOrderTotal(order),
            };
        }

        public string RemoveItemFromOrder(Guid orderId, Guid itemId)
        {
            var order = LoadFullOrder(orderId);
            if (order == null)
            {
                new OrderDetailsResponse { Result = "Failed - No Order for Given Id" };
            }
            var item = order.OrderItems.FirstOrDefault(x => x.Id == itemId);
            if (order == null)
            {
                return "Failed - Item not in given Order";
            }
            lock (SBContext.Context)
            {
                SBContext.Context.OrderItems.Remove(item);
                SBContext.Context.SaveChanges();
            }
            return "Success";
        }

        private decimal GetOrderTotal(Orders order)
        {
            return order.OrderItems.Sum(item => item.Product.Price + item.Extras.Sum(itemExtras => itemExtras.Extra.Price));
        }

        private Orders LoadFullOrder(Guid id)
        {
            var order = SBContext.Context.Orders.FirstOrDefault(o => o.Id == id);
            if (order == null)
            {
                return null;
            }

            if (order.OrderItems == null)
            {
                order.OrderItems = SBContext.Context.OrderItems.Where(x => x.OrderId == order.Id).ToArray();
            }

            foreach (var item in order.OrderItems)
            {
                if (item.Extras == null)
                {
                    item.Extras = SBContext.Context.OrderItemsExtra.Where(x => x.OrderItemId == item.Id).ToArray();
                }
                if (item.Product == null)
                {
                    item.Product = SBContext.Context.Products.FirstOrDefault(x => x.Id == item.ProductId);
                }
                foreach(var itemExtra in item.Extras)
                {
                    if (itemExtra.Extra == null)
                    {
                        itemExtra.Extra = SBContext.Context.Extras.FirstOrDefault(x => x.Id == itemExtra.ExtraId);
                    }
                }
            }

            return order;
        }
    }
}
﻿using SB.BL.Objects.Response.OrderDetails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Objects.Response
{
    public class OrderDetailsResponse : BaseResponse
    {
        public IEnumerable<OrderProduct> Products { get; set; }
        public decimal Total { get; set; }
        public bool Completed { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Objects.Response
{
    public class BaseResponse
    {
        public string Result { get; set; }
    }
}

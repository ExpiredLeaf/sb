﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Objects.Response.OrderDetails
{
    public class OrderExtra
    {
        public string ExtraName { get; set; }
        public decimal ExtraPrice { get; set; }
    }
}

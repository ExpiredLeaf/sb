﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Objects.Response.OrderDetails
{
    public class OrderProduct
    {
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public IEnumerable<OrderExtra> OrderExtras { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Objects.Request
{
    public class CompleteOrderRequest
    {
        public decimal AmountPaid { get; set; }
    }
}

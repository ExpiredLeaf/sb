﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.BL.Objects.Request
{
    public class AddItemToOrderRequest
    {
        public Guid ProductId { get; set; }
        public List<Guid> ExtrasIds { get; set; }
    }
}

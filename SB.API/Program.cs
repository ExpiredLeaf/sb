using Microsoft.EntityFrameworkCore;
using SB.DB.Data;
using Lamar.Microsoft.DependencyInjection;
using SB.BL.Interfaces;
using SB.BL.Impl;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<SBContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Host.UseLamar((context, registry) => 
{
    registry.For<ICategoryBL>().Use<CategoryBL>();
    registry.For<IProductsBL>().Use<ProductsBL>();
    registry.For<IOrdersBL>().Use<OrdersBL>();
    registry.For<IExtrasBL>().Use<ExtrasBL>();
});

var context = builder.Services.BuildServiceProvider().GetRequiredService<SBContext>();
SBInitializer.Initialize(context);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
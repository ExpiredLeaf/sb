﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SB.BL.Interfaces;
using SB.BL.Objects.Request;
using SB.BL.Objects.Response;

namespace SB.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly ILogger<OrdersController> _logger;
        private readonly IOrdersBL _iOrders;

        public OrdersController(ILogger<OrdersController> logger, IOrdersBL iOrders)
        {
            _logger = logger;
            _iOrders = iOrders;
        }

        [HttpGet]
        [Route("CreateOrder")]
        public CreateOrderResponse CreateOrder()
        {
            return _iOrders.CreateOrder();
        }

        [HttpPost]
        [Route("AddItem/{id}")]
        public string AddItem(Guid id, AddItemToOrderRequest request)
        {
            return _iOrders.AddItemToOrder(id, request);
        }

        [HttpGet]
        [Route("RemoveItem/{orderId}/{itemId}")]
        public string RemoveItemFromOrder(Guid orderId, Guid itemId)
        {
            return _iOrders.RemoveItemFromOrder(orderId, itemId);
        }

        [HttpPost]
        [Route("CompleteOrder/{id}")]
        public CompleteOrderResponse CompleteOrder(Guid id, CompleteOrderRequest request)
        {
            return _iOrders.CompleteOrder(id, request);
        }

        [HttpGet]
        [Route("CancelOrder/{id}")]
        public string CancelOrder(Guid id)
        {
            return _iOrders.CancelOrder(id);
        }

        [HttpGet]
        [Route("GetOrderDetails/{id}")]
        public OrderDetailsResponse GetOrderDetails(Guid id)
        {
            return _iOrders.GetOrderDetails(id);
        }
    }
}

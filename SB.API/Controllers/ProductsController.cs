﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SB.BL.Interfaces;
using SB.DB.Models;

namespace SB.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly IProductsBL _iProduct;

        public ProductsController(ILogger<ProductsController> logger, IProductsBL iproducts)
        {
            _logger = logger;
            _iProduct = iproducts;
        }

        [HttpPost]
        [Route("Add")]
        public string Add(Products extra)
        {
            return _iProduct.Add(extra);
        }

        [HttpGet]
        [Route("ListAll")]
        public List<Products> GetAll()
        {
            return _iProduct.ListAll();
        }

        [HttpGet]
        [Route("Get/{id}")]
        public Products Get(Guid id)
        {
            return _iProduct.GetById(id);
        }

        [HttpPost]
        [Route("Update/{id}")]
        public string Update(Guid id, Products product)
        {
            return _iProduct.Update(id, product);
        }

        [HttpGet]
        [Route("Delete/{id}")]
        public string Delete(Guid id)
        {
            return _iProduct.Delete(id);
        }
    }
}

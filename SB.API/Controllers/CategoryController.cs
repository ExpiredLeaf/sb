﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SB.BL.Impl;
using SB.BL.Interfaces;
using SB.DB.Models;

namespace SB.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ILogger<CategoryController> _logger;
        private readonly ICategoryBL _iCategory;

        public CategoryController(ILogger<CategoryController> logger, ICategoryBL icategory)
        {
            _logger = logger;
            _iCategory = icategory;
        }

        [HttpPost]
        [Route("Add")]
        public string Add(Category category)
        {
            return _iCategory.Add(category);
        }

        [HttpGet]
        [Route("ListAll")]
        public List<Category> GetAll()
        {
            return _iCategory.ListAll();
        }

        [HttpGet]
        [Route("Get/{id}")]
        public Category Get(Guid id)
        {
            return _iCategory.GetById(id);
        }

        [HttpPost]
        [Route("Update/{id}")]
        public string Update(Guid id, Category category)
        {
            return _iCategory.Update(id, category);
        }

        [HttpGet]
        [Route("Delete/{id}")]
        public string Delete(Guid id)
        {
            return _iCategory.Delete(id);
        }
    }
}

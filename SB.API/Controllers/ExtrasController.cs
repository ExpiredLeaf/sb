﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SB.BL.Interfaces;
using SB.DB.Models;

namespace SB.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ExtrasController : ControllerBase
    {
        private readonly ILogger<ExtrasController> _logger;
        private readonly IExtrasBL _iExtras;

        public ExtrasController(ILogger<ExtrasController> logger, IExtrasBL iextras)
        {
            _logger = logger;
            _iExtras = iextras;
        }

        [HttpPost]
        [Route("Add")]
        public string Add(Extras extra)
        {
            return _iExtras.Add(extra);
        }

        [HttpGet]
        [Route("ListAll")]
        public List<Extras> GetAll()
        {
            return _iExtras.ListAll();
        }

        [HttpGet]
        [Route("Get/{id}")]
        public Extras Get(Guid id)
        {
            return _iExtras.GetById(id);
        }

        [HttpPost]
        [Route("Update/{id}")]
        public string Update(Guid id, Extras extra)
        {
            return _iExtras.Update(id, extra);
        }

        [HttpGet]
        [Route("Delete/{id}")]
        public string Delete(Guid id)
        {
            return _iExtras.Delete(id);
        }
    }
}
